// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ETMGameMode.generated.h"

UCLASS(minimalapi)
class AETMGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AETMGameMode();
};



